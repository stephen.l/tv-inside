# TV Inside

TV Inside est un site web de gestion de séries, qui permet aux utilisateurs de voir des résumés de séries, mettre des commentaires, et lire les avis de la rédaction.


TV Inside est un projet scolaire réalisé par mon groupe lors du marathon du web 2019.

Le marathon du Web est un projet de l'IUT de Lens qui mixe les départements informatique et mmi (multimédia et internet) dans des équipes qui doivent produire un site web en 34h.

Ce projet est tel que nous l'avons produit en 34h.


# Lancer le site

TV Inside est projet basé sur le framework Laravel. Vous avez besoin d'avoir **php et composer ainsi que mySql** d'installer.

Pour lancer le site web en local, vous devez vous placer à la racine du projet, ensuite créer un fichier **.env** à la racine du projet en vous basant sur le **.env.example** et remplir la partie DB (connection,host,port,database,username et password).

Maintenant vous pouvez lancer les commandes (sous Linux):

- "composer install" à lancer une seule fois.
- "php artisan serve" à lancer à chaque fois que vous voulez lancer le site.

